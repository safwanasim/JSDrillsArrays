const map = require("../map");
const items = [1, 2, 3, 4, 5, 5];
const expectedOutput = [1,4,9,16,25,25]

const result = map(items, (element)=>{
    return element*element
})

if(JSON.stringify(result) == JSON.stringify(expectedOutput)){
    console.log(result)
    console.log("TEST CASE PASSED 1")
}else{
    console.log("testFailed")
}

//Second Test Case from here--------------------------------------------
let numbers = [1, 4, 9]
let result2 = map(numbers, function(num) {
    return Math.sqrt(num)
})

let expectedOutput2 = [1,2,3]
if(JSON.stringify(result2) == JSON.stringify(expectedOutput2)){
    console.log(result2)
    console.log("TEST CASE PASSED 2")
}else{
    console.log("testFailed")
}

//Third Test Case from here -------------------------------------------
let kvArray = [{key: 1, value: 10},
    {key: 2, value: 20},
    {key: 3, value: 30}]

let reformattedArray = map(kvArray,obj => {
let rObj = {}
rObj[obj.key] = obj.value
return rObj
})
// reformattedArray is now [{1: 10}, {2: 20}, {3: 30}],

if(JSON.stringify([{1: 10}, {2: 20}, {3: 30}])==JSON.stringify(reformattedArray)){
    console.log(reformattedArray)
    console.log("TEST CASE PASSED 3")
}else{
    console.log("testFailed")
}

//Test Case 4 starts here ------------------------------------------

//Mapping an array of numbers using a function containing an argument
//The following code shows how map works when a function requiring one argument is used with it. The argument will automatically be assigned from each element of the array as map loops through the original array.

let numbers1 = [1, 4, 9]
let doubles = map(numbers1, function(num) {
  return num * 2
})

// doubles is now   [2, 8, 18]
// numbers is still [1, 4, 9]

if(JSON.stringify(doubles)==JSON.stringify([2,8,18])){
    console.log(doubles)
    console.log("TEST CASE PASSED 4")
}
else{
    console.log("TEST FAILED")
}