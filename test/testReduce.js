const reduce = require("../reduce");
const items = [1, 2, 3, 4, 5, 5];
const expectedOutput = 20

const result = reduce(items, (startingValue, element) => {
    return startingValue + element
})

if (result == expectedOutput) {
    console.log("Test Case 1 Passed")
} else {
    console.log("testFailed")
}
//Test Case 2 (MDN) -------------------------------------------------------------
let result2 = reduce([0, 1, 2, 3], function (previousValue, currentValue) {
    return previousValue + currentValue
}, 0)
if (result2 == 6) {
    console.log("Test Case 2 Passed")
} else {
    console.log("testFailed")
}

//Test Case 3 (MDN) -------------------------------------------------------------
// Sum of values in an object array
// To sum up the values contained in an array of objects, you must supply an initialValue, so that each item passes through your function.

let initialValue = 0
let sum = reduce([{
    x: 1
}, {
    x: 2
}, {
    x: 3
}], function (previousValue, currentValue) {
    return previousValue + currentValue.x
}, initialValue)

if (sum == 6) {
    console.log("Test Case 3 Passed")
} else {
    console.log("testFailed")
}

//Test Case 4 (MDN)--------------------------------------------------------------
//Counting instances of values in an object
let names = ['Alice', 'Bob', 'Tiff', 'Bruce', 'Alice']
let countedNames = reduce(names, function (allNames, name) {
    if (name in allNames) {
        allNames[name]++
    } else {
        allNames[name] = 1
    }
    return allNames
}, {})
// countedNames is:
// { 'Alice': 2, 'Bob': 1, 'Tiff': 1, 'Bruce': 1 }
if (JSON.stringify({
        'Alice': 2,
        'Bob': 1,
        'Tiff': 1,
        'Bruce': 1
    }) == JSON.stringify(countedNames)) {
    console.log("TEST CASE 4 Passed")
} else {
    console.log("Test Case 4 Failed")
}

//Test Case 5 (MDN)-------------------------------------------------------------------
//Grouping objects by a property
let people = [{
        name: 'Alice',
        age: 21
    },
    {
        name: 'Max',
        age: 20
    },
    {
        name: 'Jane',
        age: 20
    }
];

function groupBy(objectArray, property) {
    return reduce(objectArray, function (acc, obj) {
        let key = obj[property]
        if (!acc[key]) {
            acc[key] = []
        }
        acc[key].push(obj)
        return acc
    }, {})
}

let groupedPeople = groupBy(people, 'age')
// groupedPeople is:
// {
//   20: [
//     { name: 'Max', age: 20 },
//     { name: 'Jane', age: 20 }
//   ],
//   21: [{ name: 'Alice', age: 21 }]
// }
if (JSON.stringify({
        20: [{
                name: 'Max',
                age: 20
            },
            {
                name: 'Jane',
                age: 20
            }
        ],
        21: [{
            name: 'Alice',
            age: 21
        }]
    }) == JSON.stringify(groupedPeople)) {
    console.log("TEST CASE 5 Passed")
} else {
    console.log("Test Case 4 Failed")
}