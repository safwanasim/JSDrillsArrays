const find = require("../find");
const items = [1, 2, 3, 4, 5, 5];
const expectedOutput = 3

const result = find(items, (number) => {
    if (number == 3) return true
    else return false
})

if (result == expectedOutput) {
    console.log("testPassed")
} else {
    console.log("testFailed")
}

//Test Case 2 (MDN) ---------------------------
//Find an object in an array by one of its properties
const inventory = [{
        name: 'apples',
        quantity: 2
    },
    {
        name: 'bananas',
        quantity: 0
    },
    {
        name: 'cherries',
        quantity: 5
    }
];

function isCherries(fruit) {
    return fruit.name === 'cherries';
}

if (JSON.stringify(find(inventory, isCherries) == JSON.stringify({
        name: 'cherries',
        quantity: 5
    }))) {
    console.log("Test Case 2 Passed")
} else {
    console.log("testFailed")
}



//Test Case 3 (MDN) ---------------------------------
//Find a prime number in an array
//The following example finds an element in the array that is a prime number (or returns undefined if there is no prime number):

function isPrime(element, index, array) {
  let start = 2;
  while (start <= Math.sqrt(element)) {
    if (element % start++ < 1) {
      return false;
    }
  }
  return element > 1;
}

if ((find([4, 6, 8, 12],isPrime) == undefined) && find([4, 5, 8, 12],isPrime) == 5) {
    console.log("Test Case 3 Passed")
} else {
    console.log("testFailed")
}


// console.log(find([4, 6, 8, 12],isPrime)); // undefined, not found
// console.log(find([4, 5, 8, 12],isPrime)); // 5