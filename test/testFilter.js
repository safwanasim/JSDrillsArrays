const filter = require("../filter");

const items = [1, 2, 3, 4, 5, 5];
const expectedOutput = [2, 4]

const result = filter(items, (number) => {
    if (number % 2 == 0) return true
    else return false
})

if (JSON.stringify(result) == JSON.stringify(expectedOutput)) {
    console.log(result)
    console.log("Test Case 1 Passed")
} else {
    console.log("testFailed")
}

//Test Case 2 (MDN)--------------------------------------------
// Filtering out all small values
// The following example uses filter() to create a filtered array that has all elements with values less than 10 removed.

function isBigEnough(value) {
    return value >= 10
}

let filtered = filter([12, 5, 8, 130, 44], isBigEnough)
// filtered is [12, 130, 44]
if (JSON.stringify(filtered) == JSON.stringify([12, 130, 44])) {
    console.log("Test Case 2 Passed")
} else {
    console.log("testFailed")
}

//Test Case 3 (MDN) ---------------------------------------------
// Find all prime numbers in an array
// The following example returns all prime numbers in the array:

const array = [-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

function isPrime(num) {
    for (let i = 2; num > i; i++) {
        if (num % i == 0) {
            return false;
        }
    }
    return num > 1;
}

const resultIsPrime = filter(array, isPrime)
if (JSON.stringify(resultIsPrime) == JSON.stringify([2, 3, 5, 7, 11, 13])) {
    console.log("Test Case 3 Passed")
} else {
    console.log("testFailed")
}

//Test Case 4 (MDN) ---------------------------------------------
//Filtering invalid entries from JSON
//The following example uses filter() to create a filtered json of all elements with non-zero, numeric id.

let arr = [{
        id: 15
    },
    {
        id: -1
    },
    {
        id: 0
    },
    {
        id: 3
    },
    {
        id: 12.2
    },
    {},
    {
        id: null
    },
    {
        id: NaN
    },
    {
        id: 'undefined'
    }
]

let invalidEntries = 0

function filterByID(item) {
    if (Number.isFinite(item.id) && item.id !== 0) {
        return true
    }
    invalidEntries++
    return false;
}

let arrByID = filter(arr, filterByID)
if (JSON.stringify(arrByID) == JSON.stringify([{
        id: 15
    }, {
        id: -1
    }, {
        id: 3
    }, {
        id: 12.2
    }])) {
    console.log("Test Case 4 Passed")
} else {
    console.log("testFailed")
}


//----------------------------------------------------------------------------
//Test Case 5 (MDN)

// Searching in array
// Following example uses filter() to filter array content based on search criteria.

let fruits = ['apple', 'banana', 'grapes', 'mango', 'orange']

/**
 * Filter array items based on search criteria (query)
 */
function filterItems(arr, query) {
    return filter(fruits, function (el) {
        return el.toLowerCase().indexOf(query.toLowerCase()) !== -1
    })
}

if (JSON.stringify(filterItems(fruits, 'ap')) == JSON.stringify(['apple', 'grapes']) && JSON.stringify(filterItems(fruits, 'an')) == JSON.stringify(['banana', 'mango', 'orange'])) {
    console.log("Test Case 5 Passed")
} else {
    console.log("testFailed")
}