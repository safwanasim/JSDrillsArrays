const flatten = function (arr, flatArray = []) {
    for (let index = 0, length = arr.length; index < length; index++) {
        const value = arr[index];
        if (Array.isArray(value)) {
            flatten(value, flatArray);
        } else {
            flatArray.push(value);
        }
    }
    return flatArray;
};

module.exports = flatten