function filter(elements, cb){
    let passedElements = []
    for (let index = 0; index<elements.length; index++){
        if(cb(elements[index])){
            passedElements.push(elements[index])
        }
    } 
    return passedElements
}
module.exports = filter